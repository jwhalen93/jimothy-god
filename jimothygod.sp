#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "Jimothy"
#define PLUGIN_VERSION "0.01"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <clientprefs>

#pragma newdecls required

#define GOD_MENU_TITLE "God Mode"
#define GOD_MENU_ON "on"
#define GOD_MENU_ON_DISPLAY "On"

#define GOD_MENU_OFF "off"
#define GOD_MENU_OFF_DISPLAY "Off"

#define GOD_MENU_KILL "kill"
#define GOD_MENU_KILL_DISPLAY "Kill all players"

// check current god status
bool g_isGod[MAXPLAYERS+1];
// check if player has already become god
bool g_hasBecomeGod[MAXPLAYERS+1];

// save god status for user
Handle g_hIsGodCookie = INVALID_HANDLE;

public Plugin myinfo = 
{
	name = "Jimothy God",
	author = PLUGIN_AUTHOR,
	description = "Create gods on the server",
	version = PLUGIN_VERSION,
	url = "https://youtu.be/5Y3_rIfOOf0"
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_godmode", God_Mode_Menu, "Become a god!");
	g_hIsGodCookie = RegClientCookie("isGodCookie", "User's godmode pref'", CookieAccess_Protected); 
}
 
public void OnClientCookiesCached(int client) 
{
    char sValue[8];
	
    GetClientCookie(client, g_hIsGodCookie, sValue, sizeof(sValue));
    g_isGod[client] = (sValue[0] != '\0' && StringToInt(sValue));
    if (g_isGod[client])
    {
    	// player with god enabled has connected, smite all peasants
    	Kill_All(client, 0);
   	}
}  

public Action Toggle_God_Mode(int client, int args)
{
	g_isGod[client] = !g_isGod[client];
	// Kill all players if its the current player's first time going god
	if (g_isGod[client] == true)
	{
		ReplyToCommand(client, "Toggled godmode ON");
		if (!g_hasBecomeGod[client])
		{
			Kill_All(client, 0);
			g_hasBecomeGod[client] = true;
		}	
	} else
	{
		ReplyToCommand(client, "Toggled godmode OFF");
	}
	char sValue[8];
	IntToString(g_isGod[client], sValue, sizeof(sValue));
	SetClientCookie(client, g_hIsGodCookie, sValue);

    return Plugin_Handled;
}

public Action Kill_All(int client, int args)
{
	if (!g_isGod[client])
	{
		ReplyToCommand(client, "How did you get here?");
	}
	else 
	{
		if (!g_hasBecomeGod[client])
		{
			PrintToChatAll("A god has entered the battleground!!");
			g_hasBecomeGod[client] = true;
		}
		PrintToChatAll("Smiting the unworthy!");
		for (int i = 1; i <= MaxClients; i++)
		{
			if (IsClientInGame(i) && !g_isGod[i])
			{
				ForcePlayerSuicide(i);
			}
		}  
	}
	return Plugin_Handled;
}

public Action God_Mode_Menu(int client, int args) 
{
	Menu menu = new Menu(Menu_Callback);
	menu.SetTitle(GOD_MENU_TITLE);
	if (g_isGod[client])
	{
		menu.AddItem(GOD_MENU_OFF, GOD_MENU_OFF_DISPLAY);
		menu.AddItem(GOD_MENU_KILL, GOD_MENU_KILL_DISPLAY);
	}
	else
	{
		menu.AddItem(GOD_MENU_ON, GOD_MENU_ON_DISPLAY);
	}

	menu.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}

public int Menu_Callback(Menu menu, MenuAction action, int client, int param2)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char item[32];
			menu.GetItem(param2, item, sizeof(item));
			DisplayMenuAtItem(menu,client, GetMenuSelectionPosition(), MENU_TIME_FOREVER);
			if (StrEqual(item,GOD_MENU_ON) || StrEqual(item,GOD_MENU_OFF))
			{
				Toggle_God_Mode(client, 0);
			}
			else if (StrEqual(item,GOD_MENU_KILL))
			{
				Kill_All(client,0);
			}
			delete menu;
			God_Mode_Menu(client, 0);

		}
		case MenuAction_End:
		{
			delete menu;
		}
	}
}

public void OnClientDisconnect(int client)
{
	ResetVariables(client);
}

public void OnClientDisconnect_Post(int client)
{
	ResetVariables(client);
}

public void ResetVariables(int client)
{
	g_isGod[client] = false;
	g_hasBecomeGod[client] = false;
}
public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
}

public Action Hook_OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3])
{
	if (g_isGod[victim])
	{
		damage = 0.0;
		return Plugin_Handled;
	}
	return Plugin_Continue;
}